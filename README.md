# Migrated

This project migrated to https://codeberg.org/nerdeiro/game-cloud-sync, please, update your URLs.

### Sync your games to (almost) any cloud service

This is a script to sync your game-saves with pretty much any cloud storage service out there.
It's meant to be used with non-Steam games or Steam games that don't implement cloud backup.
It uses [Rclone](https://rclone.org/), which supports dozens ([not hyperbole](https://rclone.org/#providers)) of providers.

The motivation to create this script was to sync my copy of Stardew Valley, that I bought on GOG, 
between my desktop and my Steam Deck. GOG has cloud saves, but only for the Windows versions of
games when you launch them from their Galaxy client.

This is where this script and [Lutris](https://lutris.net/) come in. The role Lutris will play
here, is to allow us to run a script both before and after running the game. At least
this is how it works on the desktop. On the Steam Deck, there's a little issue with
running scripts _after_ the game, more on this later.

### What you need to use this script

In no particular order:

- Lutris (optional but recommended)
- Rclone is desirable, but if you don't have it, the script will install it.
- A cloud service supported by Rclone
- A working remote configuration in Rclone

If you're syncing between two computers, getting Lutris and Rclonei is as easy as installing them
using your distro's package manager. On the Steam Deck, Lutris have to be installed using
Flatpak, after changing to Desktop mode. Rclone is a little bit more challenging, since it's
not on Flathub and the Deck ships with a read-only Root (/) file-system, so using Pacman
to get it is not an option.

Thankfully, the creators of Rclone have a statically compiled binary on the official website,
which means you can run it on the Deck without any issues, only you'll have to put it
somewhere on the `$HOME` directory. The script tries to look for it on the system $PATH, then
on the same directory the script was run from. If none is found, it downloads the latest
version on the script's directory.

Before using the script on the Steam Deck, it's recommended that you configure Rclone on a desktop,
just so you have a working config file that you can copy to the Deck. To configure Rclone, follow
for your remote cloud service. It's important that you can successfuly run a copy or sync between the
two sides. Lets say that you run `rclone copy mycloud:somedir/ ~/tmp/someotherdir`, if the 
command finishes and the files show up in `~/tmp/someotherdir`, then you're good to go.

Here's an example Rclone config for Nextcloud:

```
[nextcloud]
type = webdav
url = https://nextcloud.com/nextcloud/remote.php/dav/files/user/
vendor = nextcloud
bearer_token = aaaaa-bbbbb-ccccc-ddddd-eeeee
```

### Getting the script

Just clone it from this repo with the command

    git clone https://gitlab.com/Nerdeiro/game-cloud-sync.git

Once it's cloned, you can use it right away. On a Steam Deck, you can either use `scp`
to copy the script from your PC or change to desktop mode, open a terminal and clone
directly, since SteamOS ships with Git pre-installed.

### Using the script on the command line

These are the command line options for the script:

- -g : Name of the game config file (without `.conf`
- -o : Operation to perform. It can be upload or download
- -f : Force overwrite of newer files, this is an unsafe operation that can cause loss of game progress.
- -v : Sets Rclone to verbose mode

#### Desktop configuration

Before using it the first time, you need to create one configuration file called `sync-game.conf` pointing
to your cloud service and one for each game you want to sync. These files should be on
the [XGD user-specific configuration directory](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html).
Usually, this directory is at `$HOME/.config`, but it can be changed by setting the variable
`$XDG_CONFIG_HOME`. To check where you should create the scripts config directory, type
the command `echo $XDG_CONFIG_HOME`. If it comes out empty, then you can use the default.

Create the directory `$XDG_CONFIG_HOME/sync_game/` or `$HOME/.config/sync_game`, if the
previous variable is unset, then create the file `sync-game.conf` on it. In this file
you should have a line like this:

    CLOUD_DIR="rclone_cloud_config:cloud_dir"

Where `rclone_cloud_config` is the name you gave to your Rclone configuration and `:cloud_dir` is a
subdirectory on the service where your games will be saved. If you want your stuff on the root directory
of the service, don't put anything after the collon.

The configs for your games should be on the same directory, with one file per game. A game
config file must contain a line like this:

    SAVES_DIR="/full/path/to/your/saves/directory"

#### Steam Deck configuration

If you're running the script from a terminal on the Deck's Desktop Mode or via SSH, the
setup is the same as for the PC. But if you're running it from Lutris, things are a little
more complicated. On the Deck, Lutris is installed from Flatpak, which means it's sandboxed,
with it's own `$XDG_CONFIG_HOME`. Usually, this directory will be at `$HOME/.var/app/net.lutris.Lutris/config`,
so you should put the configuration files on it, instead of `$HOME/.config`.

### Using the script with Lutris

#### Desktop PC

Open Lutris and click with the right mouse button on your game, selecting "Configure" to show the screen bellow:

![screenshot of Lutris game settings](Lutris.png)

If your screen is not like that, tick the option "Advanced" on the top of the dialog,
then scroll all the way to the botton of the "System Options" tab. There you'll the boxes
to configure a *Pre-launch* and a *Post-exit* scripts.

To **download** the save files from the cloud to your computer (or Deck) fill the "Pre-launch" line with:

    sync-game.sh -g my_game -o download -v

To **upload** the save files from the computer to the cloud, fill the "Post-exit" line with:

    sync-game.sh -g my_game -o upload -v

If everything works as it should, your game should be saved on your cloud storage, ready to sync to other devices.

#### On the Steam Deck

If you are using Lutris to manage non-Steam games on the Deck, there's going to be
an issue with running Post-Exit scripts.

When you ask Lutris to add a shortcut to Steam, so you can launch the game from Game Mode,
Lutris tells Steam to run Flatpak, passing parameters so Flatpak:

1. Runs Lutris
2. Lutris runs the game

Unfortunatly, Lutris can't properly monitor the game in these circumstances, so it can't
properly run the Post-Exit script.

To fix this issue, I created a wrapper script called `game-wrapper.sh`. The script is very
simple, it just takes the game's name from the first parameter, uses it for the sync script, and everything
else is passed to Flatpak. Here it is, sans comments:


```
#!/bin/bash 

GAME=$1
shift 1

/home/deck/bin/sync-game.sh -g "${GAME}" -o download -v

flatpak $@

/home/deck/bin/sync-game.sh -g "${GAME}" -o upload -v
```

To use it, you do everything that you would normally, install the game in Lutris, add it to Steam and
go back to Game mode. No need to set up the sync script in Lutris. Oh, make sure the wrapper is installed
and is executable.

When you're back to Game Mode, find the shortcut to your game, open it but don't select Play yet.
Select the gear icon to the right and then Properties on the menu. The `TARGET` line should
say just `"flatpak"`. Change it to the full path to the wrapper script **between quotes**. Yes,
the quotes are important. Bellow, on the `LUNCH OPTIONS` line, add the name of your game's
config file **in front of** everything else. No need for quotes here. It should looke
like this:

![screenshot of Steam properties](Game_Properties.jpg)
