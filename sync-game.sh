#!/bin/bash - 
#===============================================================================
#
#          FILE: sync-game.sh
# 
#   DESCRIPTION: 
# 
#        AUTHOR: Nerdeiro da Silva <anarch157a@ninjazumbi.com>
#       CREATED: 2023-04-06 18:13:43
#      LICENSE: GNU GPL 2.x only
#===============================================================================

set -eu

# TEST_URL can be any website that we can fetch with Curl

TEST_URL="https://duckduckgo.com"
# Programs needed for this script:
#
# - Rclone: https://rclone.org/
# - Curl: https://github.com/curl/curl
# - Zenity: https://help.gnome.org/users/zenity/
#
# To install Rclone on the Steam Deck, download the staticaly compiled binary
# from the official site, unzip it and copy the 'rclone' binary to a convenient
# place.
#
# After that, edit the variable RC bellow, pointing to the FULL PATH.
#
# On a less locked desktop/notebook, just install the package from your distro,
# eg. 'apt install rclone' or 'pacman -S rclone'
CURL=$(which curl || ( echo "Curl not found" ; exit 1 ) ) || exit 1
DIALOG=$(which zenity || ( echo "Zenity not found" ; exit 1 ) ) || exit 1
# Yes, I know, those sub-shellceptions make Baby Ken Thompson cry...

CONFIG_DIR=${XDG_CONFIG_HOME:-$HOME/.config}
CONFIG_DIR="${CONFIG_DIR%/}/sync_game" # Remove any trailing slash to avoid double-slashes
LOCAL_STATUS_DIR=${CONFIG_DIR}/status

CONFIG_FILE="${CONFIG_DIR}/sync-game.conf"
[ -f ${CONFIG_FILE} ] || exit 1
source $CONFIG_FILE

[ -d ${LOCAL_STATUS_DIR} ] || mkdir -p ${LOCAL_STATUS_DIR}

# RC_PARAMS is being declared here to prevent Bash from complaining later.
# Leave it blank, but don't remove it.
RC_PARAMS="--update"
OPER=""

install_rclone() {
    # try to get Rclone
    __SCRIPT_DIR="$(dirname -- ${BASH_SOURCE})"
    curl -s -o ${__SCRIPT_DIR}/rclone-current-linux-amd64.zip \
       https://downloads.rclone.org/rclone-current-linux-amd64.zip >/dev/null 2>&1
    if [[ $? -ne 0 ]]; then
        echo "Can not download Rclone"
        exit 5
    fi
    unzip -qq -o -j ${__SCRIPT_DIR}/rclone-current-linux-amd64.zip */rclone -d ${__SCRIPT_DIR} >/dev/null 2>&1
    echo "${__SCRIPT_DIR}/rclone"
    return 0
}

locate_rclone() {
    # First, try a system-wide rclone
    if __RC=$(which rclone); then
        echo "${__RC}"
        return 0
    elif [[ -f $(dirname -- ${BASH_SOURCE})/rclone ]]; then
        # No system-wide rclone, let's try on the script's path
        echo "$(dirname -- ${BASH_SOURCE})/rclone"
        return 0
    else
        # Finaly, try to install it on the script directory
        echo $(install_rclone) || exit 5
    fi
}

test_connection() {
    # we use Curl because the Steam Deck lacks Netcat or equivalent tools.
    # it's OK to throw away the response. we only care for the exit code.
    if curl -Is "${TEST_URL}" >/dev/null 2>&1; then
        return 0
    else
        printf "No internet connection.\n" >&2
        return 1
    fi
}

usage_help() {
    echo "Usage:"
    echo " -g : Game to synchronize."
    echo " -o : Operation to perform. Can be either upload or download"
    echo " -v : Be verbose"
}

local_sync_time() {
    # $1 local sync dir
    # $2 game
    if [ -f ${1}/${2}.stat ]; then
        echo "$( cat ${1}/${2}.stat )"
    else
        echo "0"
    fi
}

cloud_sync_time() {
    # $1 cloud sync dir
    # $2 game
    if $RC copy ${1%/}/${2}.stat /tmp/ >/dev/null 2>&1 ; then
        __TIME=$( cat /tmp/${2}.stat )
        rm /tmp/${2}.stat
        echo "$__TIME"
    else
        echo "0"
    fi
}

save_sync_time() {
    # $1 Local status dir
    # $2 remote status dir
    # $3 game
    TIMESTAMP=$( date +%s )
    echo "${TIMESTAMP}" > "${1%/}/${3}.stat"
    $RC copy ${RC_PARAMS} "${1%/}/${3}.stat" "${2}/"
}

sync_game() {
    # $1 source
    # $2 destination
    $RC copy ${RC_PARAMS} "${1}" "${2}"
}

local_newer() {
    # When downloading FROM the cloud and local saves are newer,
    # Choose between downloading (progress may be lost) or cancel the sync.
    # This should be displayed when running the script BEFORE the game.
    ${DIALOG} --height=400 --width=600  --list --title "Select Action" \
        --text="Local files appears to be newer than remote ones.\nWhat do you want to do ?" \
        --radiolist --column " " --column "Action" --column "Option" \
        FALSE "download" "Download from the Cloud
(progress might be lost)" FALSE "keep" "Keep local files"
}

local_older() {
    # When uploading TO the cloud and local saves are older,
    # Choose between uploading (progress may be lost) or cancel the sync.
    # This should be displayed when running the script AFTER the game.
    ${DIALOG} --height=400 --width=600  --list --title "Select Action" \
        --text="Local files appears to be older than remote ones.\nWhat do you want to do ?" \
        --radiolist --column " " --column "Action" --column "Option" \
        FALSE "upload" "Upload to the Cloud
(progress might be lost)" FALSE "keep" "Keep remote files"
}

sync_to_cloud() {
    # Let's put the files on the cloud. First get the timestamp of the last
    # sync the local systems knows of

    __LOCAL_TIME=$(local_sync_time "${LOCAL_STATUS_DIR%/}" "${GAME}")

    # Then only proceed if we have internet
    if test_connection; then
        # Get the timestamp on the cloud
        __CLOUD_TIME=$(cloud_sync_time "${CLOUD_STATUS_DIR%/}" "${GAME}")

        # Now, if __LOCAL_TIME is less than __CLOUD_TIME, this means we're
        # uploading files that are OLDER older than what's in the cloud, which
        # we shouldn't do, because we risk losing game progress made on another computer.
        #
        if [[ $__LOCAL_TIME -lt $__CLOUD_TIME ]]; then   # Local is OLDER we should NOT UPLOAD
            __ACTION=$(local_older) || exit 2  # Show GUI to get the user decision. Exit on cancel
            [[ "$__ACTION" == "upload" ]] || exit 2  # If dialog returns anything other than 'upload', exit.
        fi
        # If we got to this point is because local saves are newer or the user didn't abort the script.
        # So we can upload the stuff and save the timestamp.
        sync_game "${SAVES_DIR}" "${CLOUD_GAME_DIR}"
        save_sync_time "${LOCAL_STATUS_DIR}" "${CLOUD_STATUS_DIR}" "${GAME}"
    else
        echo "No internet connection available"
        exit 3
    fi
}

sync_from_cloud() {
    # Let's put the files on the cloud. First get the timestamp of the last
    # sync the local systems knows of

    __LOCAL_TIME=$(local_sync_time "${LOCAL_STATUS_DIR%/}" "${GAME}")

    # Then only proceed if we have internet
    if test_connection; then
        # Get the timestamp on the cloud
        __CLOUD_TIME=$(cloud_sync_time "${CLOUD_STATUS_DIR%/}" "${GAME}")

        # Now, if __LOCAL_TIME is less than __CLOUD_TIME, this means we're
        # uploading files that are OLDER older than what's in the cloud, which
        # we shouldn't do, because we risk losing game progress made on another computer.
        #
        if [[ $__LOCAL_TIME -gt $__CLOUD_TIME ]]; then   # Local is NEWER we should NOT DOWNLOAD
            __ACTION=$(local_newer) || exit 2  # Show GUI to get the user decision. Exit on cancel
            [[ "$__ACTION" == "download" ]] || exit 2  # If dialog returns anything other than 'download', exit.
        fi
        # If we got to this point is because local saves are older or the user didn't abort the script.
        # So we can download the stuff and save the timestamp.
        sync_game "${CLOUD_GAME_DIR}" "${SAVES_DIR}"
        save_sync_time "${LOCAL_STATUS_DIR}" "${CLOUD_STATUS_DIR}" "${GAME}"
    else
        echo "No internet connection available"
        exit 3
    fi
}

# some boiler plate to make sure we have options
if [ ${#} -lt 1 ]; then
    usage_help
    exit 1
fi

# get source and destination from command line
while getopts 'vfg:o:' OPTIONS; do
    case "${OPTIONS}" in
        g)
            echo "Game: ${OPTARG}"
            GAME=${OPTARG}
            ;;
        o)
            echo "Operation: ${OPTARG}"
            OPER=${OPTARG}
            ;;
        f)
            echo "Force: allows overwrite"
            RC_PARAMS="${RC_PARAMS//--update/}"
            RC_PARAMS="${RC_PARAMS## }"
            RC_PARAMS="${RC_PARAMS%% }"
            ;;
        v)
            echo "Set verbose"
            RC_PARAMS="${RC_PARAMS} --progress"
            ;;
        ?)
            usage_help
            exit 1
            ;;
    esac
done

GAME_CONFIG="${CONFIG_DIR%/}/${GAME}.conf"
CLOUD_STATUS_DIR="${CLOUD_DIR%/}/sync_status"
CLOUD_GAME_DIR="${CLOUD_DIR%/}/${GAME}/"

RC=$(locate_rclone)

[ -f "${GAME_CONFIG}" ] && source "${GAME_CONFIG}" || exit 1

echo "game: $GAME"
echo "operation: $OPER"
echo "cloud status dir: $CLOUD_STATUS_DIR"
echo "local status dir: $LOCAL_STATUS_DIR"
echo "game config: $GAME_CONFIG"
echo "cloud dir: $CLOUD_DIR"
echo "saves dir: $SAVES_DIR"
echo "cloud game dir: $CLOUD_GAME_DIR"

# more boilerplate. Check if -o is valid
if [[  ${OPER} == "upload" ]];then
    sync_to_cloud
elif [[ ${OPER} == "download" ]]; then
    sync_from_cloud
else
    echo "Invalid operation"
    usage_help
    exit 1
fi

exit 0
