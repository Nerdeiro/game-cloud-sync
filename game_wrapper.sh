#!/bin/bash 

# This wrapper is just an example. Adjust the path to the sync
# script as needed. To use it with Lutris and Steam, put this script as the
# target in Steam and just add the games' config name at the beggining of
# the Launch options.
# This is necessary because Flatpaked Lutris can't run the post-exit script
# when run from Steam on the Steamdeck.
#

GAME=$1
shift 1

/home/deck/bin/sync-game.sh -g "${GAME}" -o download -v

flatpak $@

/home/deck/bin/sync-game.sh -g "${GAME}" -o upload -v
